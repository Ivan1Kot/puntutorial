﻿using UnityEngine;

public class MyCameraWork : MonoBehaviour
{
	#region Serialized Fields

	[SerializeField] private float distance = 7.0f;
	[SerializeField] private float height = 3.0f;
	[SerializeField] private Vector3 _centerOffset = Vector3.zero;
	[SerializeField] private bool _followOnStart = false;
	[SerializeField] private float smoothSpeed = 0.125f;

	#endregion

	#region Private Fields

	private Transform _cameraTransform;
	private bool _isFollowing;
	private Vector3 _cameraOffset = Vector3.zero;

	#endregion

	#region Methods

	#region Unity Methods

	private void Start()
	{
		if (_followOnStart)
		{
			OnStartFollowing();
		}
	}

	private void LateUpdate()
	{
		if (_cameraTransform == null && _isFollowing)
		{
			OnStartFollowing();
		}
		if (_isFollowing)
		{
			Follow();
		}
	}

	#endregion

	#region Public Methods

	public void OnStartFollowing()
	{
		_cameraTransform = Camera.main.transform;
		_isFollowing = true;
		Cut();
	}

	#endregion

	#region Private Methods

	private void Follow()
	{
		_cameraOffset.z = -distance;
		_cameraOffset.y = height;
		_cameraTransform.position = Vector3.Lerp(_cameraTransform.position, this.transform.position + this.transform.TransformVector(_cameraOffset), smoothSpeed * Time.deltaTime);
		_cameraTransform.LookAt(this.transform.position + _centerOffset);
	}

	private void Cut()
	{
		_cameraOffset.z = -distance;
		_cameraOffset.y = height;
		_cameraTransform.position = this.transform.position + this.transform.TransformVector(_cameraOffset);
		_cameraTransform.LookAt(this.transform.position + _centerOffset);
	}

	#endregion

	#endregion
}