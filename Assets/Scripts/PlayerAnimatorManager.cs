﻿using UnityEngine;
using Photon.Pun;

[RequireComponent(typeof(Animator))]
public class PlayerAnimatorManager : MonoBehaviourPun
{
    #region Serialized Fields

    [SerializeField] private float _directionDampTime = 0.25f;

    #endregion

    #region Private Fields

    private Animator _animator;

    #endregion

    #region Methods

    #region Unity Methods

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
        {
            return;
        }
        AnimatorStateInfo _stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
        if (_stateInfo.IsName("Base Layer.Run"))
        {
            if (Input.GetButtonDown("Fire2"))
            {
                _animator.SetTrigger("Jump");
            }
        }
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        if (v < 0)
        {
            v = 0;
        }
        _animator.SetFloat("Speed", h * h + v * v);
        _animator.SetFloat("Direction", h, _directionDampTime, Time.deltaTime);
    }

    #endregion

    #endregion
}