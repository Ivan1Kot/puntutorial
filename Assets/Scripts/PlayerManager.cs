﻿using UnityEngine;
using UnityEngine.EventSystems;
using Photon.Pun;
using Photon.Realtime;
using System.Collections;

public class PlayerManager : MonoBehaviourPunCallbacks, IPunObservable
{
	#region Serialized Fields

	[Tooltip("The Beams GameObject to control")]
	[SerializeField] private GameObject beams;

    #endregion

    #region Public Fields

    [Tooltip("The current Health of our player")]
    public float Health = 1f;
    public static GameObject LocalPlayerInstance;
    [SerializeField] public GameObject PlayerUiPrefab;

    #endregion

    #region Private Fields

    bool _isFiring;
    PhotonView _photonView;

    #endregion

    #region Methods

    #region Unity Methods

    private void Awake()
    {
        _photonView = GetComponent<PhotonView>();
        if (beams == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> Beams Reference.", this);
        }
        else
        {
            beams.SetActive(false);
        }
        if (_photonView.IsMine)
        {
            PlayerManager.LocalPlayerInstance = this.gameObject;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        MyCameraWork _cameraWork = this.gameObject.GetComponent<MyCameraWork>();
        if (_cameraWork != null)
        {
            if (_photonView.IsMine)
            {
                _cameraWork.OnStartFollowing();
            }
        }
        else
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> CameraWork Component on playerPrefab.", this);
        }
#if UNITY_5_4_OR_NEWER
        UnityEngine.SceneManagement.SceneManager.sceneLoaded += OnSceneLoaded;
#endif
        if (PlayerUiPrefab != null)
        {
            GameObject _uiGo = Instantiate(PlayerUiPrefab);
            _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
        }
        else
        {
            Debug.LogWarning("<Color=Red><a>Missing</a></Color> PlayerUiPrefab reference on player Prefab.", this);
        }
    }

    private void Update()
    {
        if (_photonView.IsMine)
        {
            ProcessInputs();
        }
        if (beams != null && _isFiring != beams.activeInHierarchy)
        {
            beams.SetActive(_isFiring);
        }
        if (Health <= 0f)
        {
            GameManager.Instance.LeaveRoom();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!_photonView.IsMine)
        {
            return;
        }
        if (!other.name.Contains("Beam"))
        {
            return;
        }
        Health -= 0.1f;
    }

    void OnTriggerStay(Collider other)
    {
        // we dont' do anything if we are not the local player.
        if (!_photonView.IsMine)
        {
            return;
        }
        // We are only interested in Beamers
        // we should be using tags but for the sake of distribution, let's simply check by name.
        if (!other.name.Contains("Beam"))
        {
            return;
        }
        // we slowly affect health when beam is constantly hitting us, so player has to move to prevent death.
        Health -= 0.1f * Time.deltaTime;
    }

#if !UNITY_5_4_OR_NEWER

    private void OnLevelWasLoaded(int level)
{
    this.CalledOnLevelWasLoaded(level);
}

#endif


    private void CalledOnLevelWasLoaded(int level)
    {
        if (!Physics.Raycast(transform.position, -Vector3.up, 5f))
        {
            transform.position = new Vector3(0f, 5f, 0f);
        }
        GameObject _uiGo = Instantiate(this.PlayerUiPrefab);
        _uiGo.SendMessage("SetTarget", this, SendMessageOptions.RequireReceiver);
    }
    #endregion

    #region Private Methods

    private void ProcessInputs()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!_isFiring)
            {
                _isFiring = true;
            }
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (_isFiring)
            {
                _isFiring = false;
            }
        }
    }

#if UNITY_5_4_OR_NEWER
    private void OnSceneLoaded(UnityEngine.SceneManagement.Scene scene, UnityEngine.SceneManagement.LoadSceneMode loadingMode)
    {
        this.CalledOnLevelWasLoaded(scene.buildIndex);
    }
#endif

    #endregion

    #region Public Methods

#if UNITY_5_4_OR_NEWER
    public override void OnDisable()
    {
        base.OnDisable();
        UnityEngine.SceneManagement.SceneManager.sceneLoaded -= OnSceneLoaded;
    }
#endif

    #endregion

    #region IPunObservable implementation

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(_isFiring);
            stream.SendNext(Health);
        }
        else
        {
            this._isFiring = (bool)stream.ReceiveNext();
            this.Health = (float)stream.ReceiveNext();
        }
    }

    #endregion

    #endregion
}