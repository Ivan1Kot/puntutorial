﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

[RequireComponent(typeof(InputField))]
public class PlayerNameInputField : MonoBehaviour
{
	#region Private Fields

	private const string _playerNamePrefKey = "PlayerName";

    #endregion

    #region Methods

    #region Unity Methods

    private void Awake()
    {
        string _defaultName = string.Empty;
        InputField _inputField = GetComponent<InputField>();
        if(PlayerPrefs.HasKey(_playerNamePrefKey))
        {
            _defaultName = PlayerPrefs.GetString(_playerNamePrefKey);
            _inputField.text = _defaultName;
        }
        PhotonNetwork.NickName = _defaultName;
    }

    #endregion

    #region Public Methods

    public void SetPlayerName(string value)
    {
        PlayerPrefs.SetString(_playerNamePrefKey, value);
        PhotonNetwork.NickName = value;
    }

    #endregion

    #endregion
}